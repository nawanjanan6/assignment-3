#include <stdio.h>

int main() {

   int num;
   printf("Input a number : ");
   scanf("%d", &num);

   if(num==0){
     printf("It is zero \n");
   }

   else if(num<0){
     printf("'%d' is negative \n", num);
   }

   else{
     printf("'%d' is positive \n", num);
   }

   return 0;
}

